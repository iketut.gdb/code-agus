import java.util.Scanner;
import java.lang.String;

public class Main {
    public static void main(String[] args) {
        Convert convert = new Convert();

        Scanner angkaPertama = new Scanner(System.in);
        System.out.println("Masukkan angka 1:");
        String nilaiPertama = angkaPertama.nextLine();
        int number = convert.stringToInterger(nilaiPertama);

        Scanner angkaKedua = new Scanner(System.in);
        System.out.println("Masukkan angka 2:");
        String nilaiKedua = angkaKedua.nextLine();
        int otherNumber = convert.stringToInterger(nilaiKedua);

        Calculate calculate = new Calculate(number, otherNumber);

        int resultAdd = calculate.add();
        int resultSubstract = calculate.substract();
        int resultDivide = calculate.divide();
        int resultMultiply = calculate.multiply();

        System.out.println("Hasil Penjumlahan : " + resultAdd);
        System.out.println("Hasil Penjumlahan : " + resultSubstract);
        System.out.println("Hasil Penjumlahan : " + resultDivide);
        System.out.println("Hasil Penjumlahan : " + resultMultiply);
    }
}

class Calculate {
    private int number;
    private int otherNumber;

    public Calculate(int number, int otherNumber){
        this.number = number;
        this.otherNumber = otherNumber;
    }
    public int add() {
        return this.number + this.otherNumber;
    }
    public int substract(){
        return this.number - this.otherNumber;
    }
    public int divide(){
        return this.number / this.otherNumber;
    }
    public int multiply(){
        return this.number * this.otherNumber;
    }
}

class Convert{
    public int stringToInterger(String n) {
        return Integer.parseInt(n);
    }
}



